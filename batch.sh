#!/bin/sh
printf 'Running... PIP apply translatation - README.md for details\n'
_sourceDir="source"
_binDir="bin/"
_distDir="dest/"
for f in `ls $_sourceDir`
   do
        echo "Working on $f \n";
        java -jar $_binDir/pip-cli-0.1.jar translate -f $_sourceDir/$f > $_distDir/${f%.*}.txt 
   done
