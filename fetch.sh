#!/bin/sh
printf 'Running... PIP apply fetcching account details - README.md for details\n'
_file=seed.csv
_endpoint=http://localhost:3000/details?id=
_sourceDir="source"
arr_accountIds=( $(tail -n +2 $_file | cut -d ',' -f1) )
arr_requestIds=( $(tail -n +2 $_file | cut -d ',' -f2) )

if [ ${#arr_accountIds[@]} = ${#arr_requestIds[@]} ] ; 
then
    for idx in ${!arr_accountIds[@]}
    do
        accountId=${arr_accountIds[idx]};
        requestId=${arr_requestIds[idx]};
        respBody=$(curl -sb -H "Accept: application/json" "$_endpoint$accountId");
        echo $respBody > $_sourceDir/${requestId}.txt
    done
else 
    echo 'Two arrays must equal length, terminate with an error'
    exit
fi

echo 'Completed!'