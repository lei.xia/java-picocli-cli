package uk.gov.dwp.health.pip.cli.utils

import spock.lang.Specification
import spock.lang.Unroll
import uk.gov.dwp.health.pip.cli.model.DrsId

class ModelValidatorTest extends Specification {

    @Unroll
    def 'should validator validate drs Id'() {
        given:
        def drsId = new DrsId()
        and:
        drsId.setRequestId(id)

        when:
        def actual = ModelValidator.validate(drsId)
        def actualMsg = ModelValidator.errorsToString()

        then:
        actual == expect

        and:
        actualMsg == msg

        where:
        id     | expect | msg
        '1234' | true   | ''
        ''     | false  | 'Request ID must be provided'
    }
}
