package uk.gov.dwp.health.pip.cli.config

import spock.lang.Specification

class EnvConfigurationPropertiesSpec extends Specification {


    def 'should return value given a key'() {
        setup:
        EnvConfigurationProperties.ENVS = [KEY1: 'KEY_VALUE_1', KEY2: 'KEY_VALUE_2']

        when:
        def actual = EnvConfigurationProperties.value('KEY1')

        then:
        actual == 'KEY_VALUE_1'
    }
}
