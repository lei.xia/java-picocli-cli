package uk.gov.dwp.health.pip.cli

import picocli.CommandLine
import spock.lang.Specification
import spock.lang.Unroll


class PipCliCommandSpec extends Specification {

    @Unroll
    def 'should run test of pip cli command'() {
        setup:
        def byteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(byteArrayOutputStream))

        and:
        def command = new PipCliCommand()
        command.verbose = verbose

        when:
        new CommandLine(command).execute()

        then:
        byteArrayOutputStream.toString() == expected

        cleanup:
        byteArrayOutputStream.close()

        where:
        verbose || expected
        true    || 'Hi!\n'
        false   || ''
    }
}