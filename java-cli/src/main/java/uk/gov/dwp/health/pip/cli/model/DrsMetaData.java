package uk.gov.dwp.health.pip.cli.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DrsMetaData {

  @JsonProperty("surname")
  private String surname;

  @JsonProperty("forename")
  private String forename;

  @JsonProperty("nino")
  private String nino;

  @JsonProperty("postcode")
  private String postcode;

  @JsonProperty("dob")
  private String dob;
}
