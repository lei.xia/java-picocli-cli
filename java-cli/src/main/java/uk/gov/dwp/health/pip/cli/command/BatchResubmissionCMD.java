package uk.gov.dwp.health.pip.cli.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import uk.gov.dwp.health.pip.cli.config.EnvConfigurationProperties;
import uk.gov.dwp.health.pip.cli.http.AccountDetailResponse;
import uk.gov.dwp.health.pip.cli.model.DrsMetaData;
import uk.gov.dwp.health.pip.cli.model.Resubmission;
import uk.gov.dwp.health.pip.cli.utils.FileUtils;
import uk.gov.dwp.health.pip.cli.utils.HttpUtils;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import static picocli.CommandLine.Command;

@Command(name = "resubmit")
public class BatchResubmissionCMD implements Runnable {

  public static final String ACCOUNT_MGR_URL =
      EnvConfigurationProperties.value("ACCOUNT_MGR_ENDPOINT");

  @Option(
      names = {"-f", "--file"},
      description = "process account by file - contains account IDs")
  private boolean byFileEnabled = false;

  @Parameters(index = "0", description = "The file contains account IDs - one row per ID")
  private File file;

  @Option(
      names = {"-i", "--id"},
      description = "process account by ID input")
  private boolean byIdEnabled = false;

  @Parameters(index = "0", description = "ID")
  private String id = "";

  private ObjectMapper objectMapper;

  public BatchResubmissionCMD() {
    objectMapper = new ObjectMapper();
  }

  @Override
  public void run() {
    if (byFileEnabled) {
      System.out.println(
          String.format(
              "Input by file enabled %s with file %s", byFileEnabled, file.getAbsolutePath()));
      Set<String> lines = FileUtils.readToSet(file.getAbsolutePath());
      extracted(lines);
    } else if (byIdEnabled) {
      id = "6022a002cf36466722ae9f31";
      System.out.println(String.format("Input by ID enabled %s or with id %s", byIdEnabled, id));
      extracted(Collections.singleton(id));
    }
  }

  private void extracted(Set<String> lines) {
    lines.forEach(
        line -> {
          AccountDetailResponse details =
              HttpUtils.get(AccountDetailResponse.class, ACCOUNT_MGR_URL, line);
          Resubmission resubmission = new Resubmission();
          resubmission.addDrs(line);
          resubmission.setDrsMetaData(
              new DrsMetaData(
                  details.getSurname(),
                  details.getForename(),
                  details.getNino(),
                  details.getPostcode(),
                  details.getDob()));
          try {
            System.out.println(objectMapper.writeValueAsString(resubmission));
            // post resubmission to submission-mgr
          } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.exit(0);
          }
        });
  }
}
