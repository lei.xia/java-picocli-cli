package uk.gov.dwp.health.pip.cli.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Resubmission {

  @JsonProperty("drs_request_ids")
  private List<DrsId> drsIds;

  @JsonProperty("drs_metadata")
  private DrsMetaData drsMetaData;

  public void addDrs(String id) {
    if (drsIds == null || drsIds.isEmpty()) {
      drsIds = new ArrayList<>();
    }
    drsIds.add(new DrsId(id));
  }

  public void populateDrsMeta(
      String surname, String forename, String nino, String postcode, String dob) {
    drsMetaData = new DrsMetaData(surname, forename, nino, postcode, dob);
  }
}
