package uk.gov.dwp.health.pip.cli.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Valid
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DrsId {

  @NotBlank(message = "Request ID must be provided")
  @JsonProperty("request_id")
  private String requestId;
}
