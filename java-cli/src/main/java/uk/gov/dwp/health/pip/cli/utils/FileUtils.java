package uk.gov.dwp.health.pip.cli.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {

  public static Set<String> readToSet(String fileName) {
    try {
      Stream<String> stream = Files.lines(Paths.get(fileName));
      return stream.collect(Collectors.toSet());
    } catch (IOException e) {
      System.err.println(String.format("Unable to read file - %s", e.getMessage()));
    }
    return Collections.emptySet();
  }

  public static String readToString(String fileName) {
    String content = "";
    try {
      content = new String(Files.readAllBytes(Paths.get(fileName)));
    } catch (IOException e) {
      System.err.println(String.format("Unable to read file - %s", e.getMessage()));
    }
    return content;
  }
}
