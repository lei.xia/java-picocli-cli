package uk.gov.dwp.health.pip.cli.http;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AccountDetailResponse {

  @JsonProperty("email")
  private String email;

  @JsonProperty("surname")
  private String surname;

  @JsonProperty("mobile_phone")
  private String mobile;

  @JsonProperty("forename")
  private String forename;

  @JsonProperty("nino")
  private String nino;

  @JsonProperty("postcode")
  private String postcode;

  @JsonProperty("dob")
  private String dob;

  @JsonProperty("language")
  private String lang;
}
