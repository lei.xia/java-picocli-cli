package uk.gov.dwp.health.pip.cli.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FilenameUtils;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import uk.gov.dwp.health.pip.cli.http.AccountDetailResponse;
import uk.gov.dwp.health.pip.cli.model.DrsMetaData;
import uk.gov.dwp.health.pip.cli.model.Resubmission;
import uk.gov.dwp.health.pip.cli.utils.FileUtils;

import java.io.File;
import java.util.Arrays;

@Command(name = "translate")
public class TranslateCommand implements Runnable {

  private static ObjectMapper objectMapper;

  static {
    objectMapper = new ObjectMapper();
  }

  @Option(
      required = true,
      names = {"-f", "--file"},
      description = "process account details by file")
  private boolean byFileEnabled = false;

  @Parameters(index = "0", description = "The file contains claimant details")
  private File file;

  @Override
  public void run() {
    if (byFileEnabled) {
      try {
        if (file.isFile()) {
          extracted(file);
        } else if (file.isDirectory()) {
          String[] list = file.list();
          Arrays.stream(list)
              .forEach(
                  path -> {
                    File f = new File(path);
                    try {
                      extracted(f);
                    } catch (JsonProcessingException e) {
                      e.printStackTrace();
                    }
                  });
        }
      } catch (JsonProcessingException e) {
        e.printStackTrace();
        System.exit(0);
      }
    }
  }

  private void extracted(File f) throws JsonProcessingException {
    final var jsonContent = FileUtils.readToString(f.getAbsolutePath());
    final var details = objectMapper.readValue(jsonContent, AccountDetailResponse.class);
    final var requestId = FilenameUtils.removeExtension(f.getName());
    Resubmission resubmission = new Resubmission();
    resubmission.addDrs(requestId);
    resubmission.setDrsMetaData(
        new DrsMetaData(
            details.getSurname(),
            details.getForename(),
            details.getNino(),
            details.getPostcode(),
            details.getDob()));
    System.out.println(objectMapper.writeValueAsString(resubmission));
  }
}
