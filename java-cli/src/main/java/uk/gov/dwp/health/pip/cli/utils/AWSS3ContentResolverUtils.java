package uk.gov.dwp.health.pip.cli.utils;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;

public class AWSS3ContentResolverUtils {

  private static S3Client S3CLIENT;

  static {
    S3CLIENT = S3Client.builder().region(Region.EU_WEST_2).build();
  }

  public void resolveS3Object(String bucket, String key) {
    var getObjectRequest = GetObjectRequest.builder().bucket(bucket).key(key).build();
    ResponseInputStream<GetObjectResponse> targetObject = S3CLIENT.getObject(getObjectRequest);
    targetObject.response();
  }
}
