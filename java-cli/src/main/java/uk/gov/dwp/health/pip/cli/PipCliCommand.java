package uk.gov.dwp.health.pip.cli;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import uk.gov.dwp.health.pip.cli.command.BatchResubmissionCMD;
import uk.gov.dwp.health.pip.cli.command.TranslateCommand;

@Command(
    name = "pip-cli",
    description = "PIP CLI to perform resubmission task",
    subcommands = {BatchResubmissionCMD.class, TranslateCommand.class},
    mixinStandardHelpOptions = true)
public class PipCliCommand implements Runnable {

  @Option(
      names = {"-v", "--verbose"},
      description = "...")
  boolean verbose;

  public static void main(String[] args) {
    int exitCode;
    exitCode = new CommandLine(new PipCliCommand()).execute(args);
    System.exit(exitCode);
  }

  public void run() {
    if (verbose) {
      System.out.println("Hi!");
    }
  }
}
