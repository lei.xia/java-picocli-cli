package uk.gov.dwp.health.pip.cli.utils;

import uk.gov.dwp.health.pip.cli.http.JSONResponseHandler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;

public class HttpUtils {

  private static HttpClient client;

  public HttpUtils() {
    client = HttpClient.newHttpClient();
  }

  public static <T> T get(Class<T> type, String url, String... params) {
    try {
      var uri = new URI(url + params[0]);
      var request = HttpRequest.newBuilder().GET().uri(uri).build();
      return client.send(request, new JSONResponseHandler<>(type)).body().get();
    } catch (IOException | InterruptedException | URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }
}
