package uk.gov.dwp.health.pip.cli.config;

import java.util.Map;

public class EnvConfigurationProperties {

  private static Map<String, String> ENVS = System.getenv();

  public static String value(String key) {
    return ENVS.get(key);
  }
}
