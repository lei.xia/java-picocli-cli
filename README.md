## PIP-Apply resubmission CLI tooling 

The tool enables PIP-apply team to resubmist failed submission/drs request semi-automatically 
(as a result of a failure of DWP systems). 

## Summary


## Organization
```
.
|____bin
| |____pip-cli-0.1.jar # java CLI translate / resubmit DRS-request
|____seed.csv # file contains `Acccount_ID | Failed_DRS_REQUEST_ID` 
|____source   # directory contains files (account detail data) in JSON, the file name is Failed_DRS_REQUEST_ID.txt
|____README.md   
|____fetch.sh     # shell script fetches Account_Meta_Details from ms-account-mgr
|____translate.sh # shell script produces drs submission requests to ms-submission-mgr in a batch
|____dest     # directory contains file (submission payload in JSON). 
|____java-cli # java CLI
```